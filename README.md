# Sudoku

## Description
A Sudoku Game!<br>
There are currently 5 different versions available:<br>
![alt text](sudoku/images/main_menu.png)

Their respective givens (choices for the Sudoku Cells) are:
<table>
<tr>
<th>SIZE
<th>NAME
<th>GIVENS
<tr>
<th>4x4
<th>Warmup
<th>[1-4]
<tr>
<th>9x9
<th>Normal Sudoku
<th>[1-9]
<tr>
<th>16x16
<th>Hexadoku
<th>[1-9,A-G]
<tr>
<th>25x25
<th>Alphadoku
<th>[A-Y]
</table>



## Installation
Currently this project works on Linux (and should work for Posix Systems in general). On native Windows it does not work, due to the use of Posix Libraries for the C extension. However, it can run both on WSL(Windows Subsystem for Linux) and Cygwin

<h2>Linux</h2>

<h4>Prerequisities</h4>
Make sure you have python3, pip and gcc installed. <br>
If not, for example for Ubuntu/Debian:

```sh
sudo apt update && sudo apt install build-essential python3 python3-pip
```

Also for the compilation of the C extension, the header file Python.h must be in your include path<br>
For example in Ubuntu/Debian you can install python3.x-dev, where x is your version of python. On other platforms this might be python3-devel

```sh
sudo apt install python3-dev
```
<h4>Install</h4>
Download the zip/tar file and extract to a folder my_folder_for_sudoku.<br> Alternatively, if git is installed, you can clone the git repository from gitlab:

```sh
git clone https://gitlab.com/games201/sudoku.git my_folder_for_sudoku
```
cd to the above folder. This is the folder that contains setup.py

```sh
cd my_folder_for_sudoku
```
and install the game:<br>

```sh
pip install --user .
```
<h4>Set Path</h4>
When you install programs with pip and the --user flag, it installs them into ~/.local/bin, which isn't in the path by default. To add ~/.local/bin to your path:

```sh
printf "PATH=\$PATH:~/.local/bin\n" >> ~/.${SHELL##*/}rc && source  ~/.${SHELL##*/}rc
```

<h4>Play</h4>
Assuming that this is now in your path, you can launch the game with:

```sh
play-sudoku
```
and that's all there is to it!


<h2>Windows</h2>
You cannot install the game in native Windows for the moment. However, it installs both in Windows Subsystem for Linux (WSL) and Cygwin

<h4>WSL</h4>
The installation is the same as the one for Linux. However, you must additionally ensure that you can display GUI apps from WSL. This is the default for Windows 11 with WSL2, but even in Windows10 you can do this, installing an X-server and displaying there. There are a lot of online references to show this, for example you can see [this guide from techcommunity.microsoft.com](https://techcommunity.microsoft.com/t5/windows-dev-appconsult/running-wsl-gui-apps-on-windows-10/ba-p/1493242)

<h4>Cygwin</h4>
It works after adding the required packages to cygwin, pex gcc, pthreads, python3.x (python3.8 was tested) python3.x-pip, python3.x-wheel, python3.x-devel ,python3.x-pygame and also xorg -the x server for the display. Now follow the linux instructions to install the game. Start the xserver:

```sh
startxwin &
```

and the game with

```sh
DISPLAY=:0.0 play-sudoku
```


## Gameplay
You can play with the mouse, or inside the game (not at the main-menu) also with the keyboard.
<h3>In Game Buttons

<table>
<tr>
<th>BUTTON
<th>FUNCTION
<tr>
<th>(Toggle) Insert Marks/Choice
<th>Marks are pencil Marks, press this to insert them!
<tr>
<th>(Toggle) Pause/Resume
<th>Press this to pause, and stop your time. Not allowed to cheat!
<tr>
<th>New Game
<th>This one too hard? Try again!
<tr>
<th>Main Menu
<th>Ready for another challenge?
<tr>
<th>Quit
<th>Too much Sudoku for one day!

</table>



<h3>Keys


<table>
<tr>
<th>KEY
<th>FUNCTION
<tr>
<th>Arrows
<th>Board Navigation- select Square
<tr>
<th>[1-9,A-Y]
<th>Insert Choice/Mark at selected Square
<tr>
<th>Delete
<th>Delete Choice/Marks from selected Square
<tr>
<th>Esc
<th>Unselect Square and Button
<tr>
<th>Tab
<th>Switch between Buttons
<tr>
<th>Enter
<th>Press highlighted Button
</table>

## Support
You can contact me at
<email>christos.bougas@protonmail.com


## Roadmap
This is just a fun project, I do not know whether it goes any further! Maybe write/train a more sophisticated engine to solve/generate the puzzles would be a little too much for a game of this scale. However, pure python code for the solve/generate of sudoku.puzzle so that I get rid of my C extension and it also works on all platforms is something to be done at some point!

## Contributing
Whoever is interested in changing the solve/generate methods in sudoku.puzzle or anything else really, feel free to do so!

With this said, if you intend to release the GIL on the given C extension library, my rand_r random_seed is not thread safe, and should be protected with a lock. 

## Acknowledgements

This game uses [pygame](https://www.pygame.org/) and the c code was interfaced with [swig](https://swig.org/)
