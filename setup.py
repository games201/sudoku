#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sudoku
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import sys
from distutils.core import Extension, setup

cudoku_module = Extension('sudoku._cudoku',
                          sources=[
                                   "c"+os.path.sep+"cudoku_interface.c",
                                   "c"+os.path.sep+"cudoku_wrap.c",
                                   "c"+os.path.sep+"generate.c",
                                   "c"+os.path.sep+"solve.c",
                                   "c"+os.path.sep+"structs.c"
                                   ],
                          include_dirs=['include'],
                          extra_compile_args=['-pthread'])


setup (       
       packages=['sudoku'],
       py_modules = ["sudoku.cudoku"],
       ext_modules = [cudoku_module],       
       install_requires=['pygame']
       )
