#include "cudokuengine.h"
static unsigned random_seed=1;

unsigned* get_seed(){
    return &random_seed;
}
void set_seed(unsigned seed){
    random_seed=seed;
}
void randomize(int seed){
    if(seed<0){random_seed=time(0);}
    else{random_seed=seed;}
}

unsigned my_sqrt_ceil(unsigned n){
    unsigned i=0;
    if(((int) n) <=0){return 0;}
    while(i*i<n){i++;}
    return i;
}

/*
order:
1. grid_verify_level(g) and set it to g
2. make_random_map(g)
3. cells from map(q,m)
4. init_constrains_from_values(con,g)
5. init_queue_from_counts(q,m)
*/
/*PUZZLE*/
void free_puzzle(puzzle* p){
    /*fprintf(stderr,"free_puzzle:p==%p\n",(void*)p);*/
    if(p==NULL){return;}
    /*fprintf(stderr,"free_puzzle:p->givens=%p\n",(void*)p->givens);*/
    if(p->givens!=NULL){free(p->givens);}
    /*fprintf(stderr,"free_puzzle:p->solution=%p\n",(void*)p->solution);*/
    if(p->solution!=NULL){free(p->solution);}
    /*fprintf(stderr,"free_puzzle:p->alternative=%p\n",(void*)p->alternative);*/
    if(p->alternative!=NULL){free(p->alternative);}
    /*fprintf(stderr,"free_puzzle:free(p)\n");*/
    free(p);
}

int get_given(puzzle* p,int k){
    if(!p||!p->givens||(unsigned)k>=p->side*p->side){return -1;}
    return p->givens[k];
}
int get_solution(puzzle* p,int k){
    if(!p||!p->solution||(unsigned)k>=p->side*p->side){return -1;}
    return p->solution[k];
}
int get_alternative(puzzle* p, int k){
    if(!p||!p->alternative||(unsigned)k>=p->side*p->side){return -1;}
    return p->alternative[k];
}
int p_initial_level(puzzle* p){
    return p->level;
}
int p_side(puzzle* p){
    return (int) p->side;
    
}
int is_uc(puzzle* p){
    return p->unique_choice;
}
int is_unique(puzzle* p){
    return p->unique;
}
int is_solvable(puzzle* p){
    return p->solvable;
}
int is_correct(puzzle* p){
    return p->correct;
}
 

/*GRID*/

/*
int get_side(grid* g){
    return (int) g->side;
}
int set_side(grid *g, int side){
    if
}
*/

grid *alloc_grid(unsigned side, int* givens){
    int k, val;grid * g;
    if  (!((int) side>=1)
                || 
    (my_sqrt_ceil(side)*my_sqrt_ceil(side)!=side)){
        return NULL;
    }
    g= malloc(sizeof(grid));
    g->side=side;   
    g->givens=malloc(side*side*sizeof(int));
    if(!givens){
        memset(g->givens,0,side*side*sizeof(int));
        return g;
    }
    for(k=0;k<(int)(side*side);k++){
        val=givens[k];
        if((unsigned)val>side){
            free(g->givens);free(g);return NULL;
        }
        g->givens[k]=val;

    }
    g->side=side;
    return g;
}
void free_grid(grid* g){
    if(!g){return;}
    if(g->givens){free(g->givens);}
    free(g);
}

/**/
int grid_verify_level(grid* g){
    unsigned k, total_squares, givens,val;
    givens=0;
    if(!g||(int)g->side<=0){return -1;}
    total_squares=g->side*g->side;
    for (k=0;k<total_squares;k++){
        val=g->givens[k];
        if (val>g->side || val<0){return -1;}
        else if(val){givens++;}
    }
    return givens;
}
int *make_random_map(grid *g,int level){
	int j=0;int k=0;
    int total_squares=g->side*g->side;
	int *m=malloc(total_squares*sizeof(int));
	int *n=malloc(total_squares*sizeof(int));
	memset(n,0,total_squares*sizeof(int));
	j=rand_r(&random_seed)%total_squares;
	while(k<total_squares- level){
		while(g->givens[j]||n[j]){
			j++;j=j%total_squares;
		}
		n[j]=1;m[j]=k;k++;
		j=rand_r(&random_seed)%total_squares;
	}
	while(k<total_squares){
		while(!g->givens[j]||n[j]){
			j++;j=j%total_squares;
		}
		n[j]=1;m[j]=k;k++;
		j=(j+1)%total_squares;
	}
	free(n);
	return m;
}
int* inverse_map(int* map,unsigned total_squares){
    int k=0;
    int *inverse=malloc(total_squares*sizeof(int));
    while(k<(int)total_squares){
        inverse[map[k]]=k;
        k++;
    }
    return inverse;
}
/*view queue--DEBUG*/
void values_queue(cell *q, int * values, unsigned total_squares){
    int j;
    for(j=0;j<(int)total_squares;j++){
        values[j]=q[j].val;
    }
    return;
}
void choices_queue(cell *q, int *choices, unsigned total_squares){
    int j;

    for(j=0;j<(int)total_squares;j++){
        choices[j]=q[j].choices;
    }
    return;
}
/*only 4 reconstruction--wrong!!!*/
void map_from_cells(int *map, cell* q, unsigned total_squares){

    cell *c=q;
    while(c-q<total_squares){
        map[c->K]=c-q;c++;
    }
    return;
}
/*CONSTRAINTS*/
constraints *new_constraints(unsigned side){
    constraints* con=malloc(sizeof(constraints));
    if(!con){return NULL;}
    con->R=malloc(side*(1+side)*sizeof(int));
    con->C=malloc(side*(1+side)*sizeof(int));
    con->B=malloc(side*(1+side)*sizeof(int));
    con->side=side;
    /*printf("conditions before\n");
    printf("ROWS\n");
    grid_pprint(stderr,con->R+side, NULL, side,'0',1,1,2);
    printf("COLS\n");
    grid_pprint(stderr,con->C+side, NULL, side,'0',1,1,2);
    printf("BOXES\n");
    grid_pprint(stderr,con->B+side, NULL, side,'0',1,1,2);*/
    memset(con->R,0,side*(1+side)*sizeof(int));
    memset(con->C,0,side*(1+side)*sizeof(int));
    memset(con->B,0,side*(1+side)*sizeof(int));
   /* printf("conditions before\n");
    printf("ROWS\n");
    grid_pprint(stderr,con->R+side, NULL, side,'0',1,1,2);
    printf("COLS\n");
    grid_pprint(stderr,con->C+side, NULL, side,'0',1,1,2);
    printf("BOXES\n");
    grid_pprint(stderr,con->B+side, NULL, side,'0',1,1,2);
    assert(0);*/
    return con;
}
void delete_constraints(constraints* con){
    if(!con){return;}
    if(con->R){free(con->R);}
    if(con->C){free(con->C);}
    if(con->B){free(con->B);}
    free(con);
}

/*level already found*/
int init_constraints_and_choices(constraints *con,
                                cell *q,
                                int* g,
                                unsigned side){
    
    int k,correct,solvable, val,*cR,*cC,*cB;
    unsigned total_squares;
    cell* c=q;
    correct=1;solvable=1;
    total_squares=(side)*(side);
    
    while(c-q<total_squares){
        if(c->val){
            k=c->K;
            val=c->val;
            cR=c_R(k, val, con);cC=c_C(k, val, con);cB=c_B(k, val, con);
            if(*cR||*cC||*cB||c->val>side){
                correct=0;
                if (*cR){con->R[k/con->side]=1;}/*the row has mistakes*/
                if (*cC){con->C[k%con->side]=1;}/*the row has mistakes*/
                if (*cB){con->B[(cB-con->B)%(con->side)]=1;}
                /*the row has mistakes*/
            }
            else{*cR=1;*cC=1,*cB=1;c->choices=1;}
            }
        else{c->choices=side;}
        c++;
    }
    c=q;
    while(c-q<total_squares){
        if(!c->val){
            k=c->K;
            for(val=1;val<=(int)side;val++){
                cR=c_R(k,val,con);cC=c_C(k,val,con);cB=c_B(k,val,con);
                if(*cR||*cC||*cB){
                    c->choices--;
                    if(c->choices==0){
                        solvable=0;
                    }
                }
                    
            }
        }
        c++;
    }
    return correct+2*solvable;
}
void box_coordinates(int i, int j, int *b, int *r, unsigned SIDE){
	unsigned box_side=my_sqrt_ceil(SIDE);
	*b=box_side*(i/box_side)+j/box_side;
	*r=box_side*(i%box_side)+j%box_side;
}
int *c_R(int k, int val,constraints *con){
    return con->R+val*(con->side)+k/(con->side);
}
int* c_C(int k, int val, constraints *con){
    return con->C+(val*(con->side)+k%(con->side));
}
int *c_B(int k, int val, constraints *con){
    int box,relevant;
    box_coordinates(k/con->side,k%con->side, &box, &relevant, con->side);
    return con->B+(val*(con->side)+box);    
}
void  apply_to_relevant_squares(
                    cell* q,constraints* con,int *g, int *map, 
 					int val, int k, void (*s),
                    void (*actions)(cell*, constraints*,int* ,int* , void*,int ,int ,int )){
    int t, i,j, side, sqrt_side,i_q_sqrt, i_m_sqrt,j_q_sqrt, j_m_sqrt;
    /*precompute*/
    side=con->side;
    t=con->side;
    i=k/t;j=k%t;
    sqrt_side=my_sqrt_ceil(t);
    i_q_sqrt=i/sqrt_side;
    i_m_sqrt=i%sqrt_side;
    j_q_sqrt=j/sqrt_side;
    j_m_sqrt=j%sqrt_side;
    

    do{
        t--;
        if(t!=j){
            actions(q,con,g,map,s,val,k,i*side+t);
        }
        if(t!=i){
            actions(q,con,g,map,s,val,k,t*side+j);
        }
        if(t/sqrt_side!=i_m_sqrt && t%sqrt_side!=j_m_sqrt){
            actions(q,con,g,map,s,val,k,
                            (i_q_sqrt*sqrt_side+t/sqrt_side)*side+(j_q_sqrt*sqrt_side+t%sqrt_side));            
        }
        
    }while(t);
    actions(q,con,g,map,s,val,k,k);
}
/*constraints*/
int constraints_is_allowed(constraints*  con, int val,int k){
    int *cR,*cC,*cB;
    cR=c_R(k, val, con);cC=c_C(k, val, con); cB=c_B(k, val, con);
    return (!(*cR||*cC||*cB||(unsigned)val>con->side));
}
void constraints_add(constraints *con,int val,int k ){
    int *cR,*cC,*cB;
    cR=c_R(k, val,con);cC=c_C(k, val, con); cB=c_B(k, val,con);
    *cR=1;*cC=1;*cB=1; 

}
void constraints_remove(constraints *con,int val,int k ){
    int *cR,*cC,*cB;
    cR=c_R(k, val, con);cC=c_C(k, val, con); cB=c_B(k, val, con);
    *cR=0;*cC=0;*cB=0;    
}

/*QUEUE-cells*/
cell *new_cells(unsigned len){
    cell* q;
    if ((int)len<0){return NULL;}
    q=malloc(len*sizeof(cell));
    memset(q,0,len*sizeof(cell));
    return q;
}
void delete_cells(cell* c){free(c);}
int cells_from_map(cell* c, int *m, grid * g){
    int k;
    /*printf("cells_from_map, map[0]=%d\n",m[0]);*/
    unsigned total_squares=g->side*g->side;
    /*k is the cell numbering, k=SIDE*i+j*/
    for(k=0;k<(int)total_squares;k++){
        c[m[k]].K=k;
        c[m[k]].val = g->givens[k];
    }
    /*printf("cells_from_map, map[0]=%d\n",m[0]);*/
    return 0;
}
/*  in the next,like arrays, 
    q[L] is not included in the init
    L is the length to be initialised to a pr queue
*/

int init_queue_from_counts(cell* q, int* m, unsigned L){
    int j;
    /*printf("init_queue_from_counts, L=%d\n",L);
    printf("init_queue_from_counts, map[0]=%d\n",m[0]);
    for(j=0;j<L;j++){
        printf("map[%d]=%d,q[%d].K=%d\n",j,m[j],m[j],q[m[j]].K);
    }*/
    
    if((int) L <=1){return 0;}
    j=L-1;
    while (j>=0){
        heapify(q,m,j,L);j--;
    }
    return 0;

}
/*supposes that the inputs are correct*/
void exchange( cell* q, int* m,unsigned l1,unsigned l2){
    
    int dummy_int;cell c;
    int k1=q[l1].K;
    int k2=q[l2].K;

    dummy_int=m[k1];    c=q[l1];
    m[k1]=m[k2];        q[l1]=q[l2];
    m[k2]=dummy_int;    q[l2]=c;
}         
/*used when a choice is made, the rel cells where the choice was allowed
should be upgraded*/
void upgrade_cell(cell * q, int* m, unsigned l1){
    int P, C;
    if((int)l1<=0){return;}
    C=l1;P=(C-1)/2;
    while (C!=0 && q[C].choices <q[P].choices){       
        exchange(q,m,P,C);
        C=P;
        P=(C-1)/2;      
    }
    return;
}
/*used when a choice is cancelled ie put back to the queue.
if it is allowed for a relevant cell, it downgrades
supposes that the tree below is already a PQ*/
void downgrade_cell(cell *q, int *m, unsigned l1, unsigned L){
    int winner, w;
    winner=l1;
    while((unsigned)winner<L){
        w=((unsigned)(2*winner+1)<L&&q[2*winner+1].choices<q[winner].choices)?2*winner+1:winner;
        w=((unsigned)(2*winner+2)<L&&q[2*winner+2].choices<q[w].choices)?2*winner+2:w;
        if(w==winner){break;}exchange(q,m,winner,w);winner=w;
    }
}
/*supposes input is correct,
  l2 is not included in the heapify:
    0<=l1<l2<=1+total_squares-level
    i am at level l, ND I ive written my pq to level+1
    want to pop myself after dealing with constraints,
    updating everybody's counts
    and before calling recursion on next level*/
void heapify(cell * q,int* m,
        unsigned l, unsigned L){
        unsigned LC,RC;
        assert(!((int)l<0||(int)L<0));
        if((int)l<0||(int)L<0){return;}
        if(l>=L){return;}
        
        LC=2*l+1; RC=2*l+2;
        if(LC<L &&(q[LC].choices <q[l].choices)){
            exchange(q, m,l,LC);heapify(q,m,LC,L);
        }
        if(RC<L &&(q[RC].choices <q[l].choices)){
            exchange(q, m,l,RC);heapify(q,m,RC,L);
        }
}
void print_queue(cell* q, int L);