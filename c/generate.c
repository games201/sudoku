#include "cudokuengine.h"
#include <time.h>

#define UNDECIDED 0
#define FIXED_FROM_CONSTRAINTS 1
#define FIXED 2

#define MIXED_REMOVALS 0
#define ONLY_SOLVE 1
#define DONT_SOLVE 2
#define CONSTRAINTS_FIRST 3


/*k is supposed to have a nonzero value*/
int remove_from_constraints(int *g, constraints* con, int k){
    int val=1;
    while(val<=(int)con->side){
        if(val!=g[k]&&constraints_is_allowed(con,val,k)){break;}
        val++;
    }
    return val;
}



#define MAX_ATTEMPTS 10000
void full_constraints(constraints* con){
    int k=con->side;
    while(k<(int)((1+con->side)*con->side)){
        con->R[k]=1;
        con->C[k]=1;
        con->B[k]=1;
        k++;
    }
}
void* RVKU3(void* gen_args ){
    
    generate_args* args=(generate_args*) gen_args;
    int k=0;int k0=0; int finished=0;
    int j=1;/*DEBUG ATTEMPTS*/
    int removed=0;
    int val=0;
    int constraints_done=0;
    int attempts=(args->max_attempts<MAX_ATTEMPTS)?args->max_attempts:MAX_ATTEMPTS;
    int strategy=((unsigned)args->strategy<=3)?args->strategy:3;
    unsigned total_squares=args->side*args->side;
    grid* g_full=NULL;
    puzzle* p=NULL;
    unsigned *seedp=get_seed();
    /*args side has been checked in generate to be correct*/
    constraints* con = new_constraints(args->side);
    int* sol=NULL;
    int* fixed=malloc(total_squares*sizeof(int));
    int require_uc=args->require_uc;
    int req_level=0;
    int best_level_generated=total_squares+1;
    int to_be_removed=0;
    int copyall=1;
    req_level=(args->level_required<=(int)total_squares)?args->level_required:(int)total_squares;
    req_level=(req_level>=0)?req_level:0;
    if(pthread_mutex_lock(args->mtx)!=0){goto cleanup;}
    if(args->time_up){pthread_mutex_unlock(args->mtx);goto cleanup;}
    pthread_mutex_unlock(args->mtx);
    while(attempts){
        /*fprintf(stderr,"ATTEMTP %d\n",j);*/
        copyall=1;
        to_be_removed=total_squares-req_level;
        memset(fixed,0,total_squares*sizeof(int));
        if(!g_full){g_full=alloc_grid(args->side,NULL);}
        else{memset(g_full->givens,0,total_squares*sizeof(int));}
        p=solve_gen(g_full,args);
        if(!p){goto cleanup;}
        if(sol!=NULL){free(sol);} sol=p->solution; p->solution=NULL;
        memcpy(g_full->givens,sol,total_squares*sizeof(int));
        full_constraints(con);
        free_puzzle(p);p=NULL;
        k0=0;
        
        if(strategy==DONT_SOLVE||strategy==CONSTRAINTS_FIRST){
            k0=rand_r(seedp)%total_squares;
            while(to_be_removed){
                k=k0;
                while(fixed[k]!=UNDECIDED){
                    k=(k+1)%total_squares;
                    if (k==k0){constraints_done=1;break;}
                }
                if(constraints_done){break;}
                val=remove_from_constraints(g_full->givens,con,k);
                if(val>(int)g_full->side){
                    
                    constraints_remove(con, g_full->givens[k], k);
                    /*fprintf(stderr,"RKVU (strategy %d): remove from constraints k=%d (%d,%d), val=%d\n", 
                    strategy,k,k/9,k%9, g_full->givens[k]);*/
                    g_full->givens[k]=0;
                    fixed[k]=FIXED;
                    to_be_removed--;
                }
                else{
                    fixed[k]=FIXED_FROM_CONSTRAINTS;
                }
                k0=rand_r(seedp)%total_squares;
            }
            
        }
        /*grid_pprint(g_full->givens,NULL,16,' ',1,1,1);*/
        
        if(pthread_mutex_lock(args->mtx)!=0){goto cleanup;}
        if(args->time_up){pthread_mutex_unlock(args->mtx);goto cleanup;}
        if(to_be_removed+req_level<best_level_generated){
            if(!args->generated){args->generated=malloc(total_squares*sizeof(int));}
            memcpy(args->generated, g_full->givens, total_squares*sizeof(int));
            copyall=0;
            
            pthread_mutex_unlock(args->mtx);
            
            best_level_generated=to_be_removed+req_level;
            copyall=0;

            if(best_level_generated<=req_level){
                pthread_cond_signal(args->cond);
                goto cleanup;
            }
        }
        else{pthread_mutex_unlock(args->mtx);}
        
        if(strategy==DONT_SOLVE){continue;}
        k0=rand_r(seedp)%total_squares;
        while(to_be_removed){
            
            k=k0;
            /*choose k to try to remove*/
            while(fixed[k]==FIXED){
                k=(k+1)%total_squares;
                if (k==k0)break;
            }
            if(fixed[k]==FIXED){break;}
            /*we cannot be in constr first this has been done*/
            if(fixed[k]==UNDECIDED&&strategy!=ONLY_SOLVE){

                val=remove_from_constraints(g_full->givens,con,k);
                if(val>(int)g_full->side){
                    /*fprintf(stderr,"RKVU (strategy %d): remove from constraints k=%d (%d,%d), val=%d\n", 
                    strategy,k,k/9,k%9, g_full->givens[k]);*/
                    g_full->givens[k]=0;
                    to_be_removed--;
                    constraints_remove(con, g_full->givens[k], k);
                    /*g_full->givens[k]=0; already done above*/
                    fixed[k]=FIXED;
                    removed=1;
                    /*to_be_removed--; already done*/
                }
                else{
                    fixed[k]=FIXED_FROM_CONSTRAINTS;
                }
            }
            if(fixed[k]==FIXED_FROM_CONSTRAINTS||strategy==ONLY_SOLVE){
                val=g_full->givens[k];
                g_full->givens[k]=0;
                removed=resolve_unique(g_full,sol,require_uc,req_level+to_be_removed-1, args);
                /*if(require_uc){printf("attempts:%d\n",attempts);}*/
                if(removed==-1){goto cleanup;}
                if(removed){
                    to_be_removed--;
                }
                else{
                    
                    constraints_remove(con, val, k);
                    /*fprintf(stderr,"RKVU (strategy %d): cant be removed k=%d (%d,%d), val=%d\n", 
                    strategy,k,k/9,k%9, val);*/
                    g_full->givens[k]=val;
                }
            }
            fixed[k]=FIXED;
            if(pthread_mutex_lock(args->mtx)!=0){goto cleanup;}
            if(args->time_up){pthread_mutex_unlock(args->mtx);goto cleanup;}
            if(!removed){
                pthread_mutex_unlock(args->mtx);
            }
            else{
                if(to_be_removed+req_level>=best_level_generated){
                    pthread_mutex_unlock(args->mtx);
                    /*copyall=1; this holds because the previous level was not copied as well*/
                }
                else{
                    if(copyall){
                        memcpy(args->generated,g_full->givens,total_squares*sizeof(int) );
                        copyall=0;
                    }
                    else{
                        args->generated[k]=0;
                        
                    }
                    pthread_mutex_unlock(args->mtx);
                    best_level_generated=to_be_removed+req_level;
                    if(best_level_generated<=req_level){
                        pthread_cond_signal(args->cond);
                        /*fprintf(stderr,"RVKU:signaling puzzle with %d givens\n", best_level_generated);*/
                        goto cleanup;
                    }
                }
            }
            
            
        }
        
        k0=rand_r(seedp)%total_squares;
        constraints_done=0;
        attempts--;j++;
    }
    cleanup:

    pthread_mutex_lock(args->mtx);
    /*fprintf(stderr,"generator checks mtx at cleanup\n");*/
    /*fprintf(stderr,"generator before checking mtx->time_up\n");*/
    finished=args->time_up;
    if(!finished){
        /*fprintf(stderr,"generator found args->time_up==0, signalling cond\n");*/
        pthread_cond_signal(args->cond);
    }   
    while(!finished){
        /*fprintf(stderr,"generator waiting\n");*/
        pthread_mutex_unlock(args->mtx);
        sleep(1);
        pthread_mutex_lock(args->mtx);
        finished=args->time_up;
    }
    /*fprintf(stderr,"generator after wait\n");*/
    pthread_mutex_unlock(args->mtx);

    pthread_mutex_destroy(args->mtx);
    pthread_cond_destroy(args->cond);
    free(args);

    if(g_full!=NULL){free_grid(g_full);}
    if(p!=NULL){free_puzzle(p);}
    if(con!=NULL){delete_constraints(con);}
    if(sol!=NULL){free(sol);}
    if(fixed!=NULL){free(fixed);}
   
    /*fprintf(stderr,"RVKU3:attempts:%d\n",1+args->max_attempts-attempts);*/    
    return NULL;
}



grid* generate(unsigned side, int ngivens,int uc_required,unsigned max_attempts, time_t time_max_sec){
    grid *g;
    generate_args *args;
    struct timespec ts;
    /*pthread_cond_t cleanup;*/
    pthread_t t;
    g=alloc_grid(side,NULL);
    if(!g){return NULL;}

    args=malloc(sizeof(generate_args));
    args->mtx=malloc(sizeof(pthread_mutex_t));
    args->cond=malloc(sizeof(pthread_cond_t)); 
    args->generated=NULL;
    pthread_mutex_init(args->mtx,NULL);
    pthread_cond_init(args->cond,NULL);
    /*cleanup = PTHREAD_COND_INITIALIZER;*/
    args->time_up=0; 
    args->side=side; 
    args->level_required=ngivens; 
    args->max_attempts=max_attempts;
    args->require_uc=uc_required;
    args->strategy=CONSTRAINTS_FIRST;
    
    pthread_mutex_lock(args->mtx);
    pthread_create(&t,NULL,RVKU3,args);
    clock_gettime(CLOCK_REALTIME,&ts);
    ts.tv_sec+=time_max_sec;

    pthread_cond_timedwait(args->cond,args->mtx,&ts);
    
    args->time_up=1;
    g->givens=args->generated;
    args->generated=NULL;
    pthread_mutex_unlock(args->mtx);
   
    if(!g->givens){
        free_grid(g);
        return NULL;
    }
    return g;
}

