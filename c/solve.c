#include "cudokuengine.h"

/*uniqueness SOS:
k=total_squares;
while(1){
    if(c+k->ch!=(c+k)->val_ch){
        copy_map_grid_and_queue_to_alternative();
        exchange(0,k);#to ensure k will choose first
        cancel_choices_0->k_of_grid_and_queue();
        init_queue_from_values();
        if(solve_from_there()){break};
        #since the original choice was already recorded, 
        #it will discard it, and start from next choice
        
    }
    k--;
}return k, alternative_solution;

*/
/*
when you go into this you have tried all grids above one choice
g->level is correct?? then the stack has size g->level
q is indeed a pq
*/
void increase_counts(cell* q, constraints* con,int* givens,int* map, void* p,int val,int k0,int k){
    unsigned L=map[k0];int num_choices;
    if(map[k]<map[k0]&&constraints_is_allowed(con,val,k)){
        q[map[k]].choices++;downgrade_cell(q, map,map[k],L);
    }
    else if(k==k0){
        /*find him a vusma to ensure he ends up top of the queue
        as he was before his previous choice*/
        num_choices=q[L].choices;
        q[L].choices=-1;
        upgrade_cell(q,map,L);
        assert(q->choices==-1);
        q->choices=num_choices;
    }
}
void CANCEL(    cell* q,
                constraints* con,
                grid* g,
                int* map,
                int L){

    int k, val;
    
    k=q[L].K;/*he's the first element out of the queue, 
                    top of the stack*/
    
    assert((unsigned)k <=g->side*g->side );
    val=q[L].val;
    if(!L){g->givens[k]=0;q->val=0;constraints_remove(con,val,k);return;}
    /*cancel the guy who failed, put him back in the queue, 
    and bring the previous guy (top of the stack) back in as queue head*/
    g->givens[k]=0;
    q->val=0;q->choices_tried=0;
    constraints_remove(con,val,k);                        
    apply_to_relevant_squares(
        q,con,g->givens,map,val,k,NULL,increase_counts);
}
/*before this function */
int FIND_NEXT_CHOICE(int previous_choice, 
                    cell* q, 
                    constraints *con, 
                    grid *g){
    int val=q->val;
    while(val<(int)con->side){
        val++;
        /*the constraints at this point dont know that i have a val*/
        if(constraints_is_allowed(con, val,q->K)){break;}
        
    }
    assert(val<=(int)con->side);
    return val;
}
void decrease_counts(cell* q, constraints* con,int* givens,int* map, void* p,int val,int k0,int k){

    if(map[k]>=map[k0]){;}/*the element is in the stack*/
    else if(constraints_is_allowed(con, val, k)){
        q[map[k]].choices--;
        upgrade_cell(q,map,map[k]);
    }
}
/*put q top of the stack(remove from q), 
update its value update relevant,
apply choice to g->givens, g->level*/
void APPLY(         cell* q,
                    constraints *con,
                    grid *g,
                    int*map, 
                    int val,
                    int L){
    int k=q->K;
    if(L==1){
	   q->val=val; 
	   g->givens[q->K]=val;
	   /*no ,choices_tried++ on the last level, 
       we do this ourselves to resolve*/
           constraints_add(con,val,q->K);
	   return;
    }
    
    assert((unsigned)k <=g->side*g->side );
    q->val=val;
    q->choices_tried++;
    g->givens[k]=val;
    /*L is sizeof queue before remove*/
    exchange(q,map,0,L-1);
    downgrade_cell(q,map,0,L-1);/**sizeof queue after remove*/
    apply_to_relevant_squares(q,con,g->givens,map,val,k,NULL,decrease_counts);
    constraints_add(con,val,k);
}
/*takes a nonfull initialised puzzle and solves it*/
int solver(cell* q,
                                constraints *con, 
                                grid *g,
                                int*map,
				                int queue_size,
                                int initial_level,
                                generate_args *args){
    
    unsigned total_squares=g->side*g->side;
    int L=queue_size;/*L is the queue size*/
    long unsigned total_counter=0; /*DEBUG*/
    int v=0;
    
    while (1){
        /*if(total_counter%100==1){fprintf(stderr,"solver");}*/
        if (args!=NULL && total_counter%100==0){
            pthread_mutex_lock(args->mtx);
            if(args->time_up){pthread_mutex_unlock(args->mtx);return -1;}
            pthread_mutex_unlock(args->mtx);
        }
        assert(q->choices_tried<=q->choices);
        if(!q->choices || q->choices==q->choices_tried){
            
            if (L==(int)total_squares-initial_level){
            /*fprintf(stderr,"solver:fail, total counter of A/C=%lu\n",total_counter);*/
                return 0;
            }
            /*this cancels previous choice, puts back 
            * to the queue the first element of the stack*/
	        CANCEL(q,con,g,map,L);L++;total_counter++;
            
            /*now sb else is at cell c, 
            the one who was popped from the stack
            */
        }
        else if (L==0){
            /*fprintf(stderr,"solver:success, total counter of A/C=%lu\n",total_counter);*/
            return 1;
        }
        else{
            v=q->val;/*the val only gets erased when topstack replaces c as q top*/
            v=FIND_NEXT_CHOICE(q->val, q,con, g);/*the constraints at this point dont know that i have a fake(previous failed) val*/
            assert(v <= (int)g->side);/*else we have cancelled the choice above*/           
            APPLY(q,con,g,map,v, L);L--;total_counter++;
            /*put q as top of the stack, update cells count and queue*/
            /*now sb else is at cell c, */
        }
    }
}

puzzle *solve(grid* g_to_solve){
    return solve_gen(g_to_solve,NULL);
}

puzzle *solve_gen(grid* g_to_solve, generate_args *args){

    int correct=0;int initial_level=0; int solved=0;int ambigous=0;
    unsigned  total_squares=0;
    constraints *con=NULL; 
    int *map=NULL; 
    cell *q=NULL; cell *uc=NULL;
    grid* g=NULL;
    puzzle *p=NULL;
    
    /*g_v_l checks if there's any other numbers than 0,,,side*/
    if (!g_to_solve){return NULL;}
    g=alloc_grid(g_to_solve->side, g_to_solve->givens);
    if(!g){return NULL;}
    initial_level=grid_verify_level(g);
    if(initial_level<0){return NULL;}
    p=malloc(sizeof(puzzle));
    
    memset(p,0,sizeof(puzzle));
    total_squares=g->side*g->side;
    p->givens=malloc(total_squares*sizeof(int));

    p->side=g->side;p->level=initial_level;
    total_squares=g->side*g->side;
    memcpy(p->givens,g->givens,total_squares*sizeof(int));
   
    map=make_random_map(g, initial_level);
    con=new_constraints(g->side);
    q=new_cells(total_squares);
    cells_from_map(q,map,g);
    correct=init_constraints_and_choices
            (con,q, g->givens, g->side)%2;
    if(!correct){p->correct=0;free_grid(g);return p;}
    p->correct=1;
    init_queue_from_counts(q,map,total_squares-initial_level);
    if(initial_level==(int)total_squares){
        p->solvable=1;p->unique=1;
        p->unique_choice=1;
        p->solution=malloc(total_squares*sizeof(int));
        memcpy(p->solution,g->givens, total_squares*sizeof(int));
        free_grid(g); return p;
    }

    solved=solver(q,con,g,map,total_squares-initial_level,initial_level,args);
    if(solved==-1){goto cleanup;}
    if(!solved){
        p->solvable=0;
        /*printf("puzzle has no solution, level=%d, initial level=%d\n", grid_verify_level(g), initial_level);
        grid_pprint(stderr,g->givens,NULL,g->side,' ',1,1,1);*/
        free_grid(g);
        
        return p;
    }
    p->solution=malloc(total_squares*sizeof(int));
    memcpy(p->solution,g->givens, total_squares*sizeof(int));
    /*CANCEL(q,con,g,map,0);CANCEL(q,con,g,map,1);
    grid_pprint(stderr,g->givens,NULL,g->side,' ',1,1,1);
    solved=solver(q,con,g,map,79);
    if(!solved){printf("this is a mistake\n");}
    else{grid_pprint(stderr,g->givens,NULL,g->side,' ',1,1,1);}*/
    p->solvable=1;
    
    memcpy(p->solution,g->givens, total_squares*sizeof(int));
    
    /*p->unique=(verify_unique)?(uniq_level==initial_level):0;
    p->unique_choice=(verify_unique_choice)?(uc_level==initial_level):0;
    p->alternative=g_alt;*/
    uc=q;

    while(uc->choices==1&&uc-q<total_squares){
        uc++;
    }
    if(uc-q>=total_squares-initial_level){p->unique_choice=1;}
    q->choices_tried++;/*cancel found solution*/
    ambigous=solver(q,con,g,map,0,initial_level,args);
    if (ambigous==-1){goto cleanup;}
    if(!ambigous){
        p->unique=1;free_grid(g);
    }
    else{
        p->alternative=g->givens;
        free(g);
    }
    cleanup:
    free(q);
    delete_constraints(con);
    free(map);
    if(args!=NULL){
        pthread_mutex_lock(args->mtx);
        if(args->time_up){free_puzzle(p);pthread_mutex_unlock(args->mtx);return NULL;}
        pthread_mutex_unlock(args->mtx);
    }
    return p;
    
    
    /**/

    /*grid_pprint(stderr,grid *g, int empty_cell, int pretty, int insert_newlines, int output_format)*/
    /*grid_pprint(stderr,p->givens,NULL,p->side, '0', 1, 1, 3);*/

}

/*no_error_checking, puzzle is supposed correct*/
int resolve_unique(grid* g_to_check, int* g_solution, int uc_required, int initial_level,generate_args *args){
    unsigned  total_squares=g_to_check->side*g_to_check->side;
    int L=total_squares-initial_level;
    constraints *con=NULL;
    int solved=0; 
    int *map=NULL; 
    cell *q=NULL;
    grid* g_test=NULL;
    int unique=1;
    int req_uc=(uc_required)?1:0;
    
    /*g_v_l checks if there's any other numbers than 0,,,side*/
    if(args!=NULL){
        pthread_mutex_lock(args->mtx);
        if(args->time_up){pthread_mutex_unlock(args->mtx);return -1;}
        pthread_mutex_unlock(args->mtx);
    }
    g_test=alloc_grid(g_to_check->side, g_to_check->givens);
    initial_level=grid_verify_level(g_to_check);   
    map=make_random_map(g_test, initial_level);
    con=new_constraints(g_test->side);
    q=new_cells(total_squares);
    cells_from_map(q,map,g_test);
    init_constraints_and_choices
            (con,q, g_test->givens, g_test->side);
    init_queue_from_counts(q,map,total_squares-initial_level);
    switch(req_uc){
        case 0:/*this is as slow as solving*/
            solved=solver(q,con,g_test,map,total_squares-initial_level, initial_level,args);
            if(solved==-1){goto cleanup;}
            q->choices_tried++;/*cancel found solution*/
            solved=solver(q,con,g_test,map,0,initial_level,args);
            if(solved==-1){goto cleanup;}
            else if(solved){unique=0;}
            
            break;
        case 1:/*but this is much quicker*/
            /*L=total_squares-initial_level; done above*/
            if(!L){break;}
            do{
                if(q->choices>1){unique=0;break;}
                APPLY(q,con,g_test,map,g_solution[q->K],L);L--;
            }while(L>0);
            break;
        default:break;
    }
    cleanup:
    
    free(q);
    delete_constraints(con);
    free(map);
    free_grid(g_test);
    if(args!=NULL){
        pthread_mutex_lock(args->mtx);
        if(args->time_up){pthread_mutex_unlock(args->mtx);return -1;}
        pthread_mutex_unlock(args->mtx);
    }
    return unique;
}

