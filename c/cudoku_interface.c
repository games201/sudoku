#include "cudokuengine.h"
#include "cudoku.h"

extern void randomize(int seed);



/*returns only the first solution it finds, 
if the grid comes from generate, uniqueness is guaranteed from there 
*/
grid *solution(grid* g_to_solve){

    int correct=0;int initial_level=0; int solved=0;
    unsigned  total_squares=0;
    constraints *con=NULL; 
    int *map=NULL; 
    cell *q=NULL;
    grid* g=NULL;
    /*g_v_l checks if there's any other numbers than 0,,,side*/
    if (!g_to_solve||!g_to_solve->givens){return NULL;}
    g=alloc_grid(g_to_solve->side, g_to_solve->givens);
    if(!g){return NULL;}
    initial_level=grid_verify_level(g);
    if(initial_level<0){free_grid(g);return NULL;}
    total_squares=g->side*g->side;
    map=make_random_map(g, initial_level);
    con=new_constraints(g->side);
    q=new_cells(total_squares);

    cells_from_map(q,map,g);
    correct=init_constraints_and_choices
            (con,q, g->givens, g->side)%2;
    if(!correct){goto cleanup;}
    init_queue_from_counts(q,map,total_squares-initial_level);
    if(initial_level==(int)total_squares){
        solved=1;
        goto cleanup;
    }
    solved=solver(q,con,g,map,total_squares-initial_level,initial_level,NULL);    
    
    cleanup:   
    free(q);
    delete_constraints(con);
    free(map);
    if(!solved){free_grid(g); return NULL;}
    return g;
}
grid* generate(unsigned side, int ngivens,int uc_required,unsigned max_attempts, long time_max_sec);

