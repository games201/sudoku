"""
       A Sudoku Game:
       
              GAME                 CHOICES
              
              4x4(warmup):         1234
              9x9(easy):           123456789
              9x9(challenge):      123456789
              16x16(hexadoku):     123456789ABCDEFG
              25x25(alphadoku):    ABCDEFGHIJKLMNOPQRSTUVWZY
          
       GAME BUTTONS:
       
              Insert Choice/Pencil marks
              Continue/Pause
              New Game
              Exit to Main Menu
              Quit
              
       GAME CONTROLS:
       
       Menu and Game Buttons, Game Board:
              mouse:               navigate game with the mouse
       
       Game Buttons:
              TAB:                 switch between Buttons
              RETURN:              press highlighted button
       
       Game Board:
              UP,DOWN,LEFT,RIGHT:  move on the board
              1-9,A-Y:             make choice/mark
              DELETE:              cancel choice/marks           
       
       Game Buttons and Board:
              ESC:          esc from highlighted square and button          
       """
from .game import WHITE,WIDTH,HEIGHT,BUTTONS_MENU,ARGS_GEN,Menu
from . import puzzle as pz
import pygame as pg
import os

def main():
    pg.init()
    pz.randomize()
    WIN=pg.display.set_mode((WIDTH,HEIGHT),pg.RESIZABLE)
    pg.display.set_caption("Sudoku")
    try:
       from pkg_resources import resource_filename as f_name
       IMG=f_name('sudoku','images'+os.path.sep+'img32x32.png')
       #print(IMG)
       gameIcon = pg.image.load(IMG)
       pg.display.set_icon(gameIcon)
    except:
       pass
    m=Menu(WIN,BUTTONS_MENU,[Menu.callback_gen(WIN,a) for a in ARGS_GEN],WHITE)
    m()

if __name__=='__main__':
    main() 
