from . import _cudoku as sc
import math
import time

def parse_dict(valid_side,d):
    cells=range(valid_side*valid_side)
    if not set(d.keys()).issubset(set(cells)):
        raise Exception('invalid cells')
    return [d.get(k,0) for k in cells]

def parse_list(valid_side,l):
    if not len(l)==valid_side*valid_side:
        raise Exception('invalid cells')
    return l
    
def randomize():
    sc.randomize(math.floor(time.time()))
    

class Grid:
    def __init__(self,side,givens=None):
        cells=[j for j in range(side*side)]
        values=[]
                        
        if not (isinstance(side,int) and side>=1):
            raise ValueError('side is an integer >=1')
        if not (math.floor(math.sqrt(side))**2 == side):
            raise Exception('side must be a perfect square')        
        if not givens:
            values=[0 ]*len(cells)
        else:
            try:
                if isinstance(givens, list):
                    values=parse_list(side,givens)
                elif isinstance(givens, tuple):
                    values=parse_list(side,list(givens))
                elif isinstance(givens, dict):
                    values=parse_dict(side,givens)
                else:
                    raise Exception('givens can be a list, tuple or dict')
            except Exception as e:
                raise e
            else:
                for k in cells:
                    if not values[k]:
                        values[k]=0
                if not set(values).issubset(set(range(side+1))):
                    raise Exception('invalid givens')                
        self.__dict__['side']=side
        self.__dict__['cells']=cells
        self.__dict__['givens']={k:values[k] for k in cells}
            
        locate={}
        sqrtside=math.floor(math.sqrt(side))
        for k in cells:
            i=k//side
            j=k%side
            b=(i//sqrtside)*sqrtside+(j//sqrtside)
            locate[k]=(i,j,b)
        self.__dict__['loc']=locate
        self.__dict__['R']={i:[] for i in range(side)}
        self.__dict__['C']={j:[] for j in range(side)}
        self.__dict__['B']={b:[] for b in range(side)}
        for k in cells:
            self.R[self.loc[k][0]].append(k)
            self.C[self.loc[k][1]].append(k)
            self.B[self.loc[k][2]].append(k)
    

    def wrap(g):
        l=[]
        givens=None
        side=-1
        if not g:
            return None
        try:
            givens=sc.grid_givens_get(g)
            side=sc.grid_side_get(g)
        except Exception as e:
            raise e
        else:
            if side<=0 or not givens:
                return None
            for k in range(side**2):
                l.append(sc.intArray_getitem(givens,k))
        return Grid(side,l)
    
    def unwrap(self):
        if not (isinstance(self.side,int) and self.side>=1 and math.floor(math.sqrt(self.side))**2 == self.side):

            return None
        if not set(self.givens.keys()).issubset(set(range(self.side**2))):

            return None
        l=[self.givens.get(k,0) for k in range(self.side**2)]
        if not set(l).issubset(set(range(self.side+1))):

            return None
        g=sc.new_grid()
        sc.grid_side_set(g,self.side)
        givens=sc.grid_givens_get(g)
        sc.delete_intArray(givens)
        givens=sc.new_intArray(self.side**2)
        for k in range(self.side**2):
            sc.intArray_setitem(givens,k,self.givens[k])
        sc.grid_givens_set(g,givens)
        return g
        
    def __str__(self):
        return self.to_string()
            
    def to_string(self,empty=' ',pretty=True, format_length=False):
        if not format_length:
            if (not isinstance(empty,str) or empty == 0):
                raise Exception('invalid empty cell')
            rows=[]
            d=max(len(empty),len(str(self.side)))
            sqrtside=math.floor(math.sqrt(self.side))
            tp=(d+3)*self.side*'-'
            for i in range(self.side):
                if pretty and not i%sqrtside:
                    rows.append(tp)
                s=""
                for j in range(self.side):
                    
                    if pretty and not j%sqrtside:
                        s+=' | '
                    if empty!=0 and empty!='0' and self.givens[i*self.side+j]==0:
                        s+=' {:>{}} '.format(empty,d)
                    else:
                        s+=' {:>{}} '.format(self.givens[i*self.side+j],d)
                if pretty:
                    s+=' |'
                if pretty and (i+1)%sqrtside!=0:
                    s+='\n'

                rows.append(s)
            if pretty:
                rows.append(tp)
            rows.append('')
            return '\n'.join(rows)
                
        elif not isinstance(format_length,int) and format_length>0:
            raise Exception('format_length is a positive integer or 0')
        elif (not isinstance(empty,str) or len(empty)!=1 or empty.isspace()) and empty!=0:
            raise Exception('invalid empty character')
        else:
            d=max(len(str(self.side)),format_length)
            s=""
            for k in range(self.side*self.side):
                val=self.givens[k]
                if not val and not (empty in [0,'0']):
                    s+=empty
                else:
                    s+=(d-len(str(val)))*'0'+str(val)
            return s
        
    def __setattr__(self, __name, __value):
        fixed={'side','loc','R','C','B'}
        if __name in fixed:
            raise Exception("not allowed to change {}".format(__name))
        else:
            super().__setattr__(__name,__value)
    
    def solve(self):
        sol=None
        g1=self.unwrap()
        if not g1:
            return None
        g2=sc.solution(g1)
        sol=Grid.wrap(g2)
        for g in [g1,g2]:
            if g:
                givens=sc.grid_givens_get(g)        
                if givens: sc.delete_intArray(givens)
                sc.delete_grid(g)        
        return sol
            
            
    
    def generate(side, ngivens,unique_choice,max_attempts_allowed,max_time_allowed_sec):
        g=None
        gen=None
        try:
            g=sc.generate(side, ngivens,unique_choice,max_attempts_allowed,max_time_allowed_sec)
        except Exception as e:
            if g:
                givens=sc.grid_givens_get(g)        
                if givens: sc.delete_intArray(givens)
                sc.delete_grid(g)
            raise e
        else:
            if not g: return None
            givens=sc.grid_givens_get(g)
            try:
                gen=Grid.wrap(g)
            except Exception as e:
                print(e)
            if g:                    
                if givens:
                    sc.delete_intArray(givens)
                sc.delete_grid(g)
            return gen
        
