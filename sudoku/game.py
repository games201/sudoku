import sys
import math
import time
import pygame as pg
from . import puzzle as pz

WIDTH, HEIGHT= 900,800
LINE_WIDTH=1
BOLD_LINE_WIDTH=2
WHITE=(255,255,255)
LIGHT_GREY=(200,200,200)
OTHER_GREY=(150,150,150)
LIGHT_BLUE=(200,240,255)
RED=(255,0,0)
BLACK=(0,0,0)
#surfaces colors
COLOR_BCKGRND=WHITE
COLOR_GIVEN_SQUARE=LIGHT_GREY
COLOR_EMPTY_SQUARE=WHITE
COLOR_CHOSEN_SQUARE=LIGHT_BLUE
#letter colors:
COLOR_LETTER=BLACK
COLOR_MISTAKE=RED
COLOR_TEMP=OTHER_GREY
#button colors
COLOR_BUTTONS_1=LIGHT_GREY
COLOR_BUTTONS_2=OTHER_GREY
BUTTONS_MENU=["Play 4x4(warmup)","Play 9x9 (easy)","Play 9x9 (challenge)","Play 16x16 (hexadoku)","Play 25x25 (alphadoku)"]
ARGS_GEN=[(4,0,0,100,1),(9,0,1,100,1),(9,0,0,100,1),(16,0,1,100,3),(25,0,1,100,3)]

NUM_BUTTONS=5
FPS=60
NINE="123456789"
HEX="123456789ABCDEFG"
ALPHABET="ABCDEFGHIJKLMNOPQRSTUVWXY"
PAD={4: NINE[:4],9:NINE,16:HEX,25:ALPHABET}

INSERT_GIVENS=0
INSERT_TEMP=1

INSERT_GIVENS_TEMP=["Insert Choice","Insert Marks"]

def game_quit():
    pg.quit()
    sys.exit()

class Button:
    def __init__(self,window,x_y_w_h,callback_text,callback_function,callback_color,color_letters,font1):
        self.window=window
        self.font=font1
        self.text=callback_text
        self.play=callback_function
        self.paint=callback_color
        self.color_letters=color_letters
        self.hide=False        
        self.x_y_w_h=x_y_w_h
    def draw(self,mouse):
        if self.hide:
            return
        x,y,w,h=self.x_y_w_h(self.window.get_width(),self.window.get_height())
        color=self.paint(mouse)
        pg.draw.rect(self.window,color,[x,y,w,h])
        text=self.font.render(self.text(),1,self.color_letters)
        if text.get_width()>w or text.get_height()>h:
            text=pg.transform.scale(text,(w//2,h//2))
        self.window.blit(text,(x+(w-text.get_width())//2,y+(h-text.get_height())//2))
    def check_mouse(self,mouse):
        x,y,w,h=self.x_y_w_h(self.window.get_width(),self.window.get_height())
        return x<mouse[0]<x+w and y<mouse[1]<=y+h

class Menu:
    def __init__(self, window, texts, callbacks,
        color,color1=LIGHT_GREY,color2=OTHER_GREY,color_letters=BLACK
        ):
        self.window=window
        self.color=color
        Buttons=[]
        N=0
        try:
            z=zip(texts,callbacks)
            N=min(len(texts),len(callbacks))
        except Exception as e:
            raise e
        i=1
        fnt=pg.font.SysFont("comicsans",35)
        for t,c in z:
            if not isinstance(t,str):
                raise Exception
            b=Button(
                window,Menu.menu_item_pos(i,N),Menu.text_callback(t),c,None,color_letters,fnt
            )
            b.paint=Menu.paint_buttons(b,color1,color2)
            Buttons.append(b)
            i+=1
        b=Button(window,Menu.menu_item_pos(i,N),Menu.text_callback("Quit") ,game_quit,None,color_letters,fnt)
        b.paint=Menu.paint_buttons(b,color1,color2)
        Buttons.append(b)
        
        
        self.buttons=Buttons
    
    #return text
    def text_callback(text):
        def t():
            return text
        return t
    
    #position buttons
    def menu_item_pos(i,N):
        return lambda w,h:(w//4,i*h//(N+2),w//2,h//(N+4))        
    
    #callback color
    def paint_buttons(b,color1,color2):
        def painter(mouse):
            x,y,w,h=b.x_y_w_h(b.window.get_width(),b.window.get_height())
            return color1 if x<mouse[0]<x+w and y<mouse[1]<=y+h else color2
        return painter
    #callback generate
    def callback_gen(window,args):
        def generator():
            g=None
            while not g:
                g= pz.Grid.generate(*args)
            s=Sudoku(window,g,args)
            s()
        return generator
    
    def draw(self):
        self.window.fill(self.color)
        mouse = pg.mouse.get_pos()
        for b in self.buttons:
            b.draw(mouse)
    def __call__(self):
        clock=pg.time.Clock()
        run=True
        while run:
            clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
                if event.type == pg.MOUSEBUTTONDOWN:
                    mouse = pg.mouse.get_pos()
                    for b in self.buttons:
                        if b.check_mouse(mouse):
                            b.play()
            self.draw()
            pg.display.update()    


class Pad:
    def __init__(self,sudoku):
        self.s=sudoku
        self.pad=PAD[self.s.side]
    def __getitem__(self,index):
        return self.pad[index]
    def __str__(self):
        return self.pad
    #X,Y is the start of the pad, S is ihe Size of each input
    def get_X_Y_S(self,W,H):
        O = min(W//3,H//2)
        S=O//(2+self.s.sqrtside)
        X,Y=(2*W//3+S//2,S//2)
        return X,Y,S
    def draw(self,W,H):
        if self.s.insert is None: return
        X,Y,S = self.get_X_Y_S(W,H)
        for j in range(self.s.side):
            x=X+S*(j%self.s.sqrtside)
            y=Y+S*(j//self.s.sqrtside)
            color=self.s.col_givn if (j+1) not in self.s.cells[self.s.insert].temp else self.s.col_chos
            pg.draw.rect(self.s.window,color,[x,y,S,S])
            text=self.s.fnt.render(self.s.pad[j],1,self.s.col_lett)
            text=pg.transform.scale(text,(S//2,2*S//3))
            self.s.window.blit(text,(x+S//4,y+S//6))
        color=self.s.col_givn
        x,y=X,Y+S*self.s.sqrtside
        w,h=(S,2*S//3)
        pg.draw.rect(self.s.window,color,[x,y,self.s.sqrtside*S,S])
        text=self.s.fnt.render("Clear",1,self.s.col_lett)
        text=pg.transform.scale(text,(w,h))
        self.s.window.blit(text,(x+(S*self.s.sqrtside-w)//2,y+(S-h)//2))

    def getchoice(self, mouse,W,H):
        if self.s.insert is None:
            return None
        X,Y,S = self.get_X_Y_S(W,H)
        if not (X<mouse[0]<X+S*self.s.sqrtside and Y<mouse[1]<Y+S+S*self.s.sqrtside):
            return None
        if mouse[1]>Y+S*self.s.sqrtside:
            return 0
        return 1+self.s.sqrtside*((mouse[1]-Y)//S)+(mouse[0]-X)//S        
            
class Cell:
    def __init__(self,sudoku,k):
        self.s=sudoku
        self.k=k
        self.given=self.s.puz.givens.get(k,None)
        if not self.given:
            self.given=None
        self.choice=self.given
        self.mistake=False     
        self.temp=set()

        
    def draw(self,S):
        i,j=(self.k//self.s.side,self.k%self.s.side)
        x,y=(S+S*j,S+S*i)
        color=self.s.color_choices()
        self.draw_choice(x,y,S,color(self.k))
        self.draw_temp(x,y,S)
    #x,y=start of Square
    # only used inside draw
    def draw_choice(self,x,y,S,color):
        if not self.choice: return
        text=self.s.fnt.render(self.s.pad[self.choice-1],1,color)
        w,h=(S//3,2*S//3)
        text=pg.transform.scale(text,(w,h))
        self.s.window.blit(text,(x+S//3,y+S//6))
    # only used inside draw
    def draw_temp(self,x,y,S):
        l=sorted(list(self.temp))
        temp_capacity=self.s.side if self.s.side<=16 else 12
        if not l: return
        num_rows=max(2,len(l))
        num_rows=min(num_rows,self.s.sqrtside) if self.s.side<=16 else min(num_rows,3)
        num_cols=math.ceil(temp_capacity/num_rows)
        o_h=S//num_rows
        o_w=S//(2*num_rows)
        for i in range(len(l)):
            cols_full=i//num_rows
            xi,yi=(x+o_w*cols_full,y+o_h*(i%num_rows))
            if self.choice is not None and cols_full>=math.ceil(num_cols//2):
                xi=x+2*S//3+o_w*(cols_full-math.ceil(num_cols//2))
            text=self.s.fnt.render(self.s.pad[l[i]-1],1,self.s.col_temp)
            text=pg.transform.scale(text,(o_w-2,o_h-2))
            self.s.window.blit(text,(xi+1,yi+1))    
    def check_mouse(self,mouse,S):
        x,y=(S*(1+self.k%self.s.side),S*(1+self.k//self.s.side))
        return x<mouse[0]<x+S and y<mouse[1]<y+S    
class Sudoku:
    def __init__(self,window,g, args,
                 color_letter=COLOR_LETTER,color_mistake=COLOR_MISTAKE,color_temp=COLOR_TEMP,
                 color_bg=COLOR_BCKGRND,color_given=COLOR_GIVEN_SQUARE,
                 color_empty=COLOR_EMPTY_SQUARE,color_chosen=COLOR_CHOSEN_SQUARE,
                 color_buttons1=COLOR_BUTTONS_1,color_buttons2=COLOR_BUTTONS_2,font="comicsans"):
        self.window=window
        self.puz=g
        self.sol=g.solve()
        #print(self.sol)
        self.args=args
        self.mode=INSERT_GIVENS
        self.col_lett=color_letter
        self.col_mist=color_mistake
        self.col_temp=color_temp
        self.col_bgrd=color_bg
        self.col_givn=color_given
        self.col_empt=color_empty
        self.col_chos=color_chosen
        self.col_but1=color_buttons1
        self.col_but2=color_buttons2
        self.fnt=pg.font.SysFont(font,35)
        self.draw_board_and_choices=True
        self.side=self.side=self.puz.side
        self.sqrtside=math.ceil(math.sqrt(self.side))
        self.insert = None
        self.finished=False
        self.cells=[Cell(self,k) for k in self.puz.cells]
        self.pad=Pad(self)
        self.buttons=self.get_buttons()
        self.buttons[0].play=self.switch_mode_callback
        self.buttons[1].play=self.switch_pause
        self.buttons[2].play=self.new_game
        self.buttons[3].play=self.exit_to_main_menu
        self.highlighted=None
        self.time=time.time()
        self.paused=False
        
    def switch_mode_text(self):
        return INSERT_GIVENS_TEMP[0] if self.mode==INSERT_TEMP else INSERT_GIVENS_TEMP[1]

    def switch_pause_text(self):
        return "Resume" if self.paused else "Pause"
    
    #position Buttons
    def game_item(i,N):
        def pos(W,H):
            x,y=(2*W//3,H//2)
            w,h=(W//3,H//2)
            return (x+w//4,y+i*h//(N+2),w//2,h//(N+4))
        return pos
    
    def color_choices(self):
        def f(k):
            return self.col_mist if self.cells[k].mistake else self.col_lett
        return f
    
    def color_squares(self):
        def f(k):
            if self.puz.givens[k]:
                return self.col_givn
            if self.insert==k:
                return self.col_chos
            return self.col_empt
        return f       
    def paint_buttons(self,b,color1,color2):
        def painter(mouse):
            if self.highlighted is not None and self.buttons[self.highlighted] is b: return color1
            x,y,w,h=b.x_y_w_h(b.window.get_width(),b.window.get_height())
            return color1 if x<mouse[0]<x+w and y<mouse[1]<=y+h else color2
        return painter

    def get_buttons(self):
        buttons=[]
        
        b = Button(  self.window,
                        Sudoku.game_item(1,NUM_BUTTONS),
                        self.switch_mode_text,
                        None,
                        #switch_mode_callback(self),
                        None,
                        self.col_lett,
                        self.fnt
                        )
        b.paint=self.paint_buttons(b,self.col_but1,self.col_but2)
        buttons.append(b)
        
        b = Button(  self.window,
                        Sudoku.game_item(2,NUM_BUTTONS),
                        self.switch_pause_text,
                        None,
                        #switch_mode_callback(self),
                        None,
                        self.col_lett,
                        self.fnt
                        )
        b.paint=self.paint_buttons(b,self.col_but1,self.col_but2)
        buttons.append(b)
        
        b = Button(  self.window,
                        Sudoku.game_item(3,NUM_BUTTONS),
                        Menu.text_callback("New Game"),
                        None,
                        # Sudoku.new_game(self),           
                        None,
                        self.col_lett,
                        self.fnt
                        )
        b.paint=self.paint_buttons(b,self.col_but1,self.col_but2)
        buttons.append(b)
        
        b = Button(  self.window,
                        Sudoku.game_item(4,NUM_BUTTONS),
                        Menu.text_callback("Main Menu"),
                        None,
                        None,
                        self.col_lett,
                        self.fnt
                        )
        b.paint=self.paint_buttons(b,self.col_but1,self.col_but2)
        buttons.append(b)
        
        b = Button(  self.window,
                        Sudoku.game_item(5,NUM_BUTTONS),
                        Menu.text_callback("Quit"),
                        game_quit,
                        None,
                        self.col_lett,
                        self.fnt
                        )
        b.paint=self.paint_buttons(b,self.col_but1,self.col_but2)
        buttons.append(b)
        
        return buttons
    
    def draw(self,W,H,S,play_time):
        self.window.fill(self.col_bgrd)
        if not self.finished and not self.paused and self.draw_board_and_choices:
            self.draw_board(S)
            for c in self.cells: 
                c.draw(S)
            self.pad.draw(W,H)
        elif self.finished:
            self.draw_time(play_time,W,H)
            message_pos=Menu.menu_item_pos(1,NUM_BUTTONS)
            x,y,w,h=message_pos(W,H)
            text=self.fnt.render("Puzzle Complete",1,self.col_lett)
            if text.get_width()>w or text.get_height()>h:
                text=pg.transform.scale(text,(w//2,h//2))
            self.window.blit(text,(x+(w-text.get_width())/2,y+(h-text.get_height())/2))
            for i in range(len(self.buttons)):
                self.buttons[i].x_y_w_h=Menu.menu_item_pos(i+1,NUM_BUTTONS)
            self.buttons[0].hide=True
            self.buttons[1].hide=True
            self.highlighted=0
        mouse = pg.mouse.get_pos()
        for b in self.buttons:
            b.draw(mouse)
        if not self.finished:
            self.draw_time(play_time,W,H)
    
    def get_sizes(self):
        W=self.window.get_width()
        H=self.window.get_height()
        #total length of board side
        L=min(2*W//3,H)
        self.draw_board_and_choices=(L>=300)
        S = L//(1+self.side)
        return W,H,S
    
    def draw_time(self,play_time,W,H):
        f=Sudoku.game_item(NUM_BUTTONS+1,NUM_BUTTONS) if not self.finished else Menu.menu_item_pos(0,NUM_BUTTONS)
        x,y,w,h=f(W,H)
        mins,secs=(play_time//60,play_time%60)
        hrs,mins=(mins//60,mins%60)
        stime="{}:{}:{}".format(hrs,mins,secs)
        text = self.fnt.render(stime,1,self.col_lett)
        if text.get_width()>w or text.get_height()>h:
            text=pg.transform.scale(text,(w//2,h//2))
        self.window.blit(text,(x+(w-text.get_width())//2,y+(h-text.get_height())//2))
                
    def draw_board(self,S):
        lnwidth=0       
        pos=[S*n for n in range(1,self.side+2)]
        i=0
        color=self.color_squares()
        for k in range(self.side*self.side):
            pg.draw.rect(self.window,color(k),
                         [pos[k%self.side],pos[k//self.side],S,S])
        for i in range(1+self.side):
            lnwidth=LINE_WIDTH if i%self.sqrtside else BOLD_LINE_WIDTH
            pg.draw.line(self.window,BLACK,(pos[i]-(lnwidth//2),pos[0]),(pos[i]-(lnwidth//2),pos[-1]),width=lnwidth)
            pg.draw.line(self.window,BLACK,(pos[0],pos[i]-lnwidth//2),(pos[-1],pos[i]-lnwidth//2),width=lnwidth)
            i+=1


           
    #callbacks
    def new_game(self):
        g=pz.Grid.generate(*(self.args))
        while not g:
            g=pz.Grid.generate(*(self.args))
        self.puz = g
        self.sol = g.solve()
        self.cells=[Cell(self,k) for k in self.puz.cells]
        self.mode=INSERT_GIVENS
        self.insert = None
        self.finished=False
        self.highlighted=None        
        for i in range(len(self.buttons)):
            self.buttons[i].x_y_w_h=Sudoku.game_item(i+1,NUM_BUTTONS)
            self.buttons[i].hide=False
        self.paused=False
        self.time=time.time()
    
    def playtime(self):
        return self.time if (self.finished or self.paused) else time.time()-self.time
    
    def exit_to_main_menu(self):
        self.run=False
    
    def switch_pause(self):
        if self.finished:
            return
        self.time=time.time()-self.time
        self.paused = (not self.paused)
        
    def switch_mode_callback(self):
        if self.finished or self.paused:
            return
        self.mode=INSERT_TEMP if self.mode == INSERT_GIVENS else INSERT_GIVENS
    
    def check(self):
        finished=True
        mistakes=set()
        for c in self.cells:
            if c.choice is None:
                c.mistake=False
                finished=False
            else:
                i,j,b=self.puz.loc[c.k]
                rel=set(self.puz.R[i]).union(self.puz.C[j]).union(self.puz.B[b])-{c.k}
                if c.choice in set(map(lambda k: self.cells[k].choice,rel)):
                    c.mistake=True
                    mistakes.add(c.k)
                else:
                    c.mistake=False
        if mistakes:
            finished = False       
        self.finished = finished

    def transition_state_key(self,key):

        moves={
            pg.K_UP:-self.side,
            pg.K_DOWN:self.side,
            pg.K_LEFT:-1,
            pg.K_RIGHT:1
        }
        if key == pg.K_TAB:
            if self.highlighted is None:
                self.highlighted = 0
            else:
                self.highlighted=(self.highlighted+1)%NUM_BUTTONS
            #print("tab")
            return
        if key == pg.K_RETURN:
            if self.highlighted is None:
                return
            b=self.buttons[self.highlighted]
            b.play()
            #print("enter")
            return
        
        if key == pg.K_ESCAPE:
            if self.finished or self.paused:
                return
            self.insert = None
            self.highlighted = None
            return
        if key == pg.K_DELETE:
            if self.finished or self.paused:
                return
            if self.insert is not None:
                if self.mode == INSERT_GIVENS:
                    self.cells[self.insert].choice = None
                else:
                    self.cells[self.insert].temp=set()
            return
        
        if key in moves:
            if self.finished or self.paused:
                return
            move=moves[key]
            if self.insert is None:
                self.insert = 0
            else:
                self.insert =(self.insert+move)%(self.side*self.side)
            while self.puz.givens[self.insert]!=0:
                self.insert =(self.insert+move)%(self.side*self.side)
            #print("move")
            return
        if self.finished==True or self.paused==True or self.insert is None:
            return
        choice=0
        try:
            choice=1+self.pad.pad.find(chr(key).upper())
        except:
            return
        if not choice:
            return
        c=self.cells[self.insert]
        if self.mode == INSERT_GIVENS:
            c.choice=choice 
        else:
            if choice in c.temp:
                c.temp.remove(choice)
            else:
                if self.side<=16 or len(c.temp)<12:
                    c.temp.add(choice)                
        return
        
        
    
    def transition_state_mouse(self,mouse,W,H,S):
        for b in self.buttons:
            if b.check_mouse(mouse): 
                b.play()
                return
        if self.finished or self.paused: return
        for c in self.cells:
            if c.check_mouse(mouse,S):
                self.insert= c.k if self.puz.givens[c.k]==0 else None
                return
        choice= self.pad.getchoice(mouse,W,H)
        if choice is None: 
            self.insert =None
            return
        c=self.cells[self.insert]
        if choice == 0:
            if self.mode == INSERT_GIVENS:
                c.choice = None
            else:
                c.temp=set()
        else:
            if self.mode == INSERT_GIVENS:
                c.choice=None if c.choice == choice else choice 
            else:
                if choice in c.temp:
                    c.temp.remove(choice)
                else:
                    if self.side<=16 or len(c.temp)<12:
                        c.temp.add(choice)  
            
    def __call__(self):
        clock=pg.time.Clock()
        self.run=True
        while self.run:
            clock.tick(FPS)
            play_time=round(self.playtime())
            W,H,S=self.get_sizes()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
                if event.type == pg.MOUSEBUTTONDOWN:
                    mouse = pg.mouse.get_pos()
                    self.transition_state_mouse(mouse,W,H,S)                   
                if event.type == pg.KEYDOWN:
                    self.transition_state_key(event.key)
                self.check()
                if self.finished:self.time=play_time
            self.draw(W,H,S,play_time)
            pg.display.update()

   
