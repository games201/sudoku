#ifndef _CUDOKUC_H
#define _CUDOKUC_H
#include <unistd.h>

typedef struct grid{
    unsigned side;
    int *givens;
}grid;

/*
set random seed
*/
extern void randomize(int seed);
/*
solve
*/
extern grid* solution(grid *g_to_solve);

/*
generate
*/
extern grid* generate(unsigned side, int ngivens,int uc_required,unsigned max_attempts, long time_max_sec);

#endif
