#ifndef _CUDOKUENGINE_H
#define _CUDOKUENGINE_H
#include "cudoku.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <pthread.h>

typedef struct puzzle{
    unsigned side;
    unsigned level;
    int correct;
    int solvable;
    int unique;
    int unique_choice;
    int *givens;
    int *solution;
    int *alternative;
}puzzle;

typedef struct cell{
    unsigned K;/*coordinate on grid*/
    unsigned val;/*final value*/
    int choices;/*key to the pq*/
    int choices_tried;   
}cell;

typedef struct constraints{
    unsigned side;
    int *R;
    int *C;
    int *B;
}constraints;

typedef struct generate_args{
    pthread_mutex_t *mtx;
    pthread_cond_t *cond;
    /*pthread_cond_t *cleanup;*/
    int time_up;
    unsigned side;
    unsigned max_attempts;
    int level_required;
    int require_uc;
    int strategy;
    int *generated;
}generate_args;

/*helpers*/
void randomize(int seed);
void set_seed(unsigned seed);
unsigned* get_seed();


unsigned my_sqrt_ceil(unsigned n);
unsigned my_bits(unsigned n);
unsigned my_digits(unsigned S);
int read_buf(int *buf, int digits);
/*IO: input-output*/
void print_queue(cell* q, int L);

grid* grid_fread(FILE* s,unsigned side, char empty_cell, int input_format);
grid *grid_from_file(const char *filename,unsigned side,char empty_cell,int input_format);
grid *grid_from_string(const char *s,unsigned side, char empty_cell, int input_format);

int grid_read_stream( grid* g,FILE* s, char empty_cell, int input_format);
void grid_pprint(FILE *s,int* to_print,int* map, unsigned side, int empty, int pretty, int insert_newlines, int output_format);
void pprint(grid* g,int pretty, char empty_cell);
char* grid_to_string(   int* to_print,
                        int* map,
                        unsigned total_size,
                        unsigned rows, 
                        int empty_cell, 
                        int pretty, 
                        int output_format);
int grid_printf(int* to_print,
                int* map,
                unsigned total_size,
                unsigned rows, 
                int empty_cell, 
                int pretty, 
                int output_format);
/*SOLVE*/
puzzle *solve_gen(grid* g_to_solve, generate_args *args);
puzzle* solve(grid *g);

/*GENERATE*/
grid* generate(unsigned side, int ngivens,int uc_required,unsigned max_attempts, time_t time_max_sec);

/*PUZZLE*/
/*new puzzle is only supposed to come from solve*/
void free_puzzle(puzzle* p);

int get_given(puzzle* p,int index);
int get_solution(puzzle* p, int index);
int get_alternative(puzzle* p,int index);
int p_initial_level(puzzle* p);
int p_side(puzzle* p);
int is_uc(puzzle* p);
int is_unique(puzzle* p);
int is_solvable(puzzle* p);
int is_correct(puzzle* p);

void free_puzzle(puzzle* p);
/*GRID&MAP*/

grid *alloc_grid(unsigned side, int * givens);
void free_grid(grid* g);
int grid_verify_level(grid* g);
void  apply_to_relevant_squares(
                    cell* q,
                    constraints* con,
                    int *g, 
                    int *map, 
 					int val, int k, void (*s),
                    void (*actions)
        (cell*, constraints*,int* ,int* , void*,int ,int ,int ));
int *make_random_map(grid *g, int level);/*not 4 the header*/
/*only 4 reconstruction, NORMALLY MAP PRECEDES CELLS*/
void map_from_cells(int* map, cell* q, unsigned total_squares);
int* inverse_map(int* map,unsigned total_squares);
/*CONSTRAINTS*/
void box_coordinates(/*helper*/
    int i, int j, int *b, int *r, unsigned SIDE);
int* c_R(int k, int val, constraints *con);
int* c_C(int k, int val, constraints *con);
int* c_B(int k, int val, constraints *con);
constraints *new_constraints(unsigned side);
void delete_constraints(constraints* con);
/*CELLS*/
void values_queue(cell *q,int *values, unsigned total_squares);
void choices_queue(cell *q,int *choices, unsigned total_squares);
int init_constraints_and_choices(constraints *con,
                                cell *q,
                                int *g,
                                unsigned side);
int constraints_is_allowed(constraints*  con, int val,int k);
void constraints_add(constraints *con,int val,int k );
void constraints_remove(constraints *con,int val,int k );

/*QUEUE-cells*/
cell *new_cells(unsigned len);
void delete_cells(cell* c);
int cells_from_map(cell* q, int *m, grid * g);
int init_queue_from_counts(cell* q, int* m, unsigned L);
void exchange(cell* q, int* m,unsigned l1,unsigned l2);
void heapify(cell * q,int* m,unsigned l1, unsigned l2);
void upgrade_cell(cell * q, int* m, unsigned l1);
void downgrade_cell(cell *q, int *m, unsigned l1, unsigned L);
/*SOLVE*/
void increase_counts
    (cell*,constraints*,int*,int*,void*,int,int,int);
void CANCEL(cell* q,
                            constraints* con,
                            grid* g,
                            int* map,
                            int level);
void decrease_counts
    (cell*,constraints*,int*,int*,void*,int,int,int);
void APPLY(  cell* q,
                    constraints *con,
                    grid *g,
                    int*map, 
                    int val,
                    int level);
int FIND_NEXT_CHOICE(int previous_choice, 
                    cell* q, 
                    constraints *con, 
                    grid *g);

int solver(cell* q,
                                constraints *con, 
                                grid *g,
                                int*map,
                                int queue_size,
                                int initial_level,
                                generate_args *args);

int resolve_unique(grid* g_to_check, int* g_solution, int uc_required, int initial_level, generate_args *args);


#endif
